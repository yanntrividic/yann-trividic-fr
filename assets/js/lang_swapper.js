// Handling bilingual version
function swapLang(button){
    let mains = document.querySelectorAll(".main");
    mains.forEach(main => {
        if (main.style.display == "none") main.style.display = "block";
        else main.style.display = "none";
    })
    if(button) swapLangURI(button.innerText)
}

function swapLangURI(lang){
    if (history.pushState) {
        var newurl = window.location.origin + window.location.pathname + "?lang=" + lang;
        window.history.pushState({path:newurl},'',newurl);
    }
}

function isEnglish(){
    return window.location.href.split("?")[1] == "lang=en" ;
}

function getLang(elem){
    return elem.closest(".main").lang;
}

function getLocaleLang(elem) {
    return getLang(elem) == "fr" ? "fr-FR" : "en-EN" ;
}

if(isEnglish()) swapLang() ;