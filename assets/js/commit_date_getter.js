function fillWithCommitData(element) {
    fetch('https://gitlab.com/api/v4/projects/41947297/repository/commits?per_page=1', {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
        },
    })
    .then(response => response.json())
    .then(response => {
        let date = new Date(Date.parse(response[0].committed_date))
        element.href = response[0].web_url
        let lang = getLang(element);
        if(lang == "fr") {
            element.innerHTML = date.toLocaleDateString("fr-FR", {
                day: "2-digit",
                month: "2-digit",
                year: "numeric",
            })
        } else {
            element.innerHTML = date.toLocaleDateString("en-US", {
                year: "numeric",
                month: "2-digit",
                day: "2-digit",
            })
        }
    })
}

// Adding the last modified in the end
document.querySelectorAll(".commitDate").forEach((elem) => {
    fillWithCommitData(elem);
})