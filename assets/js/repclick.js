// Replace text on click
function repClick(event, append=false) {
    if(event.type == "keydown" && !(event.key == "Space" || event.key == "Enter")) return ;
    let element = event.target;
    let dataAlt = element.getAttribute("data-alt");
    if(append && element.getAttribute("value") != "appended") {
        dataAlt = element.innerHTML + dataAlt ;
        element.setAttribute("value", "appended")
    }
    element.setAttribute("data-alt", element.innerHTML);
    element.innerHTML = dataAlt;
    if (element.style.fontStyle == "italic") {
        element.style.fontStyle = "";
    } else {
        element.style.fontStyle = "italic";
    }
}