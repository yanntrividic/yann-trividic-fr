let dates = document.getElementsByTagName("code");
Array.from(dates).forEach(function (date) {
    let dateArray = date.innerHTML.split(" ")
    date.innerHTML = processDate(getLocaleLang(date), ...dateArray)
});

function processDate(locale, precision, startDate, endDate) {
    let isoStartDate = new Date(Date.parse(startDate))
    let strStartDate = getStringDateWithPrecision(precision, isoStartDate, locale)
    let strEndDate = ""
    switch (endDate) {
        case 'null':
            strEndDate = locale == "fr-FR" ? "en cours" : "ongoing"
            break;
        case undefined:
            break;
        default:
            let isoEndDate = new Date(Date.parse(endDate))
            strEndDate = getStringDateWithPrecision(precision, isoEndDate, locale)
            break;
    }
    return strStartDate + ((strEndDate == "" || strStartDate == strEndDate) ? "" : (" &#8211 " + strEndDate))
}

function getStringDateWithPrecision(precision, date, locale) {
    switch (precision) {
        case 'day':
        return date.toLocaleDateString(locale, {
            day: "numeric",
            month: "short",
            year: "numeric",
        })
        case 'month':
        return date.toLocaleDateString(locale, {
            month: "short",
            year: "numeric",
        })
        case 'year':
        return date.toLocaleDateString(locale, {
            year: "numeric",
        })
        default:
        console.error('The precision argument was incorrect.');
    }
}