class OpenDetails extends Paged.Handler {
    constructor(chunker, polisher, caller) {
        super(chunker, polisher, caller);
    }
    beforeParsed(content) {
        content.querySelectorAll("details:not(.hideForPrint)").forEach(detail => {
            var h = document.createElement('div');
            h.classList = ["details"];
            h.innerHTML = detail.innerHTML;
            detail.parentNode.replaceChild(h, detail);
        })
    }
}

Paged.registerHandlers(OpenDetails);

// Thanks to Julie Blanc: https://gitlab.com/JulieBlanc/julieblanc.gitlab.io/-/blob/master/assets/js/print.js?ref_type=heads
function printResume(){
    let paged = new Paged.Previewer();
    paged.preview();
}

class endRender extends Paged.Handler {
    constructor(chunker, polisher, caller) {
        super(chunker, polisher, caller);
    }
    afterRendered(pages){
        window.print();
    }
}
Paged.registerHandlers(endRender);

class UrlHyphenator extends Paged.Handler {
    // Hook originally developed by Nicolas Taffin
    // this let us call the methods from the the chunker,
    // the polisher and the caller for the rest of the script
    constructor(chunker, polisher, caller) {
      super(chunker, polisher, caller);
    }
  
    beforeParsed(content) {
        //add wbr to / in links
        // transformed to add ZERO WIDTH SPACE (break opportunity) &#x200B;
        const links = content.querySelectorAll(".details a");
        // console.log(links);
        for (let link of links) {
            // Rerun to avoid large spaces. Break after a colon or a double slash (//) 
            // or before a single slash (/), a tilde (~), a period, a comma, a hyphen,
            // an underline (_), a question mark, a number sign, or a percent symbol.
            const content = link.href;
            let printableUrl = content.replace(/\/\//g, "//\u200B");
            // put wbr around everything.
            //printableUrl = printableUrl.replace(/(\/|\~|\-|\=|\,|\_|\?|\#|\&|\;|\%)/g, "$1\u003Cwbr\u003E");
            printableUrl = printableUrl.replace(/(\/|\~|\-|\=|\,|\_|\?|\#|\&|\;|\%)/g, "$1\u003Cwbr\u003E");
            printableUrl = printableUrl.replace(/\./g, ".\u200B");
            let following_link = document.createElement("a")
            following_link.innerHTML = "(" + printableUrl + ")";
            following_link.href = link.href;
            following_link.setAttribute("data-print-url", printableUrl);
            following_link.classList = ["following_link"];
            link.insertAdjacentElement("afterend", following_link);
            // link.innerHTML = printableUrl;
        }
    }
}

Paged.registerHandlers(UrlHyphenator);