function edit_resume() {
    let btn = document.createElement("button")
    btn.innerHTML = "&#x274C;" ;
    btn.classList = ["closeButton"] ;

    let summaries = document.getElementsByTagName("summary");
    Array.from(summaries).forEach(function (summary) {
        summary.insertAdjacentElement("beforeend", btn.cloneNode(true))
    });

    let entries = document.querySelectorAll("code");
    entries.forEach(function (entry) {
        entry.insertAdjacentElement("beforeend", btn.cloneNode(true))
    });

    document.querySelectorAll(".closeButton").forEach(closeBtn => {
        closeBtn.addEventListener("click", hideForPrint);
    });

    document.querySelectorAll("details .content p").forEach(p => {
        p.setAttribute("contentEditable", true);
    });

    addDragEvents()
}

function hideForPrint(event) {
    event.target.parentElement.parentElement.classList.add("hideForPrint");
    event.target.innerHTML = "&#x274E;" ;
    event.target.removeEventListener("click", hideForPrint, false);
    event.target.addEventListener("click", showForPrint)
}

function showForPrint(event) {
    event.target.parentElement.parentElement.classList.remove("hideForPrint");
    event.target.innerHTML = "&#x274C;" ;
    event.target.removeEventListener("click", showForPrint, false);
    event.target.addEventListener("click", hideForPrint)
}

function addDragEvents() {
    const draggables = document.querySelectorAll('details')
    const containers = document.querySelectorAll('.main')

    draggables.forEach(draggable => {
        draggable.setAttribute("draggable", true)
        draggable.addEventListener('dragstart', () => {
            draggable.classList.add('dragging')
        })
        draggable.addEventListener('dragend', () => {
            draggable.classList.remove('dragging')
        })
    })

    containers.forEach(container => {
        container.addEventListener('dragover', e => {
            e.preventDefault()
            const afterElement = getDragAfterElement(container, e.clientY)
            const draggable = document.querySelector('.dragging')
            if (afterElement == null) {
                container.appendChild(draggable)
            } else {
                container.insertBefore(draggable, afterElement)
            }
        })
    })
}

function getDragAfterElement(container, y) {
    const draggableElements = [...container.querySelectorAll('details:not(.dragging)')]
    return draggableElements.reduce((closest, child) => {
        const box = child.getBoundingClientRect()
        const offset = y - box.top - box.height / 2
        if (offset < 0 && offset > closest.offset) {
            return {
                offset: offset,
                element: child
            }
        } else {
            return closest
        }
    }, {
        offset: Number.NEGATIVE_INFINITY
    }).element
}

// edit_resume() // needs to be activated from the browser's command line