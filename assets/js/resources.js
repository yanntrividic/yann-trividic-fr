function log_sizes(perfEntry){
    // Check for support of the PerformanceEntry.*transferSize properties and print their values
    // if supported.
    if ("transferSize" in perfEntry) {
        // console.log(perfEntry.name, perfEntry.transferSize)
        return perfEntry.transferSize;
    } else console.log("transferSize not supported");
}

async function getResourcesSize() {
    var p = performance.getEntriesByType("resource");
    var totalSize = 0;
    for (var i=0; i < p.length; i++) {
        totalSize = totalSize + log_sizes(p[i]);
    }
    return totalSize/1000 ;
}

// Helper function to get the size of a URL using Fetch API
async function getSizeOfResource(url) {
    try {
        const response = await fetch(url, { method: 'HEAD' });
        const contentLength = response.headers.get('content-length');
        return contentLength ? parseInt(contentLength, 10) : 0;
    } catch (error) {
        console.error(`Failed to fetch resource size for ${url}:`, error);
        return 0;
    }
}

// Function to get the size of the current HTML page
async function getHTMLPageSize() {
    try {
        // Use the current URL
        const url = window.location.href;

        // Fetch the HTML using a GET request to get the response body
        const response = await fetch(url, { method: 'GET' });

        // Check if the response is okay
        if (!response.ok) {
            console.error('Failed to fetch the HTML:', response.statusText);
            return 0;
        }

        // Read the response text
        const htmlText = await response.text();
        
        // Calculate the size of the HTML content in bytes
        const sizeInBytes = new TextEncoder().encode(htmlText).length;

        return sizeInBytes/1000;
    } catch (error) {
        console.error('Error fetching the HTML size:', error);
        return 0;
    }
}

window.addEventListener("load", async () => {

    let totalSizeResources = await getResourcesSize();
    let pageSize = await getHTMLPageSize();

    let totalSize = totalSizeResources + pageSize;
    console.log("pageSize", pageSize)
    console.log("totalSizeResources", totalSizeResources)

    document.querySelectorAll(".totalSize").forEach((elem) => {
        elem.innerHTML = Math.round(totalSize) + (getLang(elem) == "fr" ? "&#x202F;ko" : "kB");
    })
})