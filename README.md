# yanntrividic.fr

Site personnel de Yann Trividic : https://yanntrividic.fr \
Dépôt GitLab : https://gitlab.com/yanntrividic/yann-trividic-fr

La commande pour générer le CV est la suivante : `asciidoctor cv/index.adoc -a stylesheet!`.